// Se crea la aplicación con express
var fs = require('fs');
var Users = require('./Users.js');
var express = require("express");
var http = require("http");
var replace1 = require("replace"); 
var app =express();
var datos='';
var master = '1077300970'; //código maestro que permite reiniciar el archivo register.txt
//variables globales del estudiante
var stationArray = new Array();

stationArray.station1 = new Object();
stationArray.station1 ={studentIDout: '', assigned: '', bikeID: '', date1: '', //date global
flag:''  };
var studentIDout;
var assigned;
var bikeID;
var date1; //date global
var date2; //date local
var flag; // Esta flag es para evitar que se active la información del socket-bikeID sin que el usuario se encuentre en register.txt
//Otras variables globales
var cont = new Object();
cont={ contStudents:0, contFines:0};
var contprev={ contStudents:0, contFines:0};;
var day= new Date();



//Se revisa el archivo de ultimo día. Si es diferente al de hoy entonces se reinician los contadores y se escribe el día actual en el archivo
var readLastDay=fs.readFile('lastday.txt',function(err,chunk){

			console.log('fecha de hoy'+day.toDateString(+'\n\r'));

			if(day.toDateString()!==chunk.toString()){
			cont={contStudents:0, contFines:0};
			var WriteStatistics=fs.createWriteStream("statistics.txt", {flags: 'a', encoding: null});
			WriteStatistics.write(day.toDateString()+','+contprev.contStudents+','+contprev.contFines+';');
			var WriteLastDay=fs.createWriteStream("lastday.txt", {flags: 'w', encoding: null});
			WriteLastDay.write(day.toDateString());
			}
});

/*****Frame generation*****/
app.set('port', process.env.PORT || 3000);
app.set('ip',  '192.168.0.100');
app.get("/", function(req, res){
	res.send("page");
});



//Generation of server with app in http
var server = http.createServer(app).listen(app.get('port'), app.get('ip'), function(){
	var host =server.address().address;
	var port =server.address().port;
	console.log('Example app listening at http://%s:%s',host, port);	
});


// Generation of the socket for communication
var io = require('socket.io').listen(server);

io.on("connection",function (socket){
	
	
	
	console.log('New user connected');
	socket.on('message', function(msg){
		console.log(msg);
		io.emit('message','This is an answer from server');
	});
	socket.on('studentID', function(studentID){
		//	
	
			
		//Empieza función dentro de recibir studentID
	
		console.log('Recibí : '+ studentID+ '\n\r');
		//Verificación de usuario maestro
		if(studentID!=master){

		//if(datoprev!=studentID ){
	//Empieza comprobación de DataBase
		var readDataBase=fs.readFile('database.txt',function(err,chunk){
			datos='';
			datos+=chunk;
		
		if(datos.toString().indexOf(studentID)>=0){
				console.log('Usted es un usuario válido');
				
	//Ahora que es VÁLIDO se abre el archivo de multas fine.txt
					var readFine=fs.readFile('fine.txt', function(err,chunk){
					datos='';
					datos+=chunk;
	//Se comprueba con el siguiente if si hay o no multa
					if(datos.toString().indexOf(studentID)>=0){
						console.log('Usted tiene multa, no puede usar el sistema');
						io.emit('audio', 'fine');						
	//En este else continúa la ejecución del programa con studentValidation						
					}else{
						console.log('Usted NO tiene multa');						
						
						var readRegister=fs.readFile("register.txt", function(err,chunk){
						datos='';						
						datos+=chunk;
						console.log('Aquí esto es studentId: '+studentID+'\n\r');
						if(datos.toString().indexOf(studentID)<0){
							//Aquí se crea el nuevo usuario ya que no está en el registro
							console.log('Se creará un nuevo usuario para: '+studentID+ '\n\r');
							io.emit('audio', 'valid');
							var WriteRegister=fs.createWriteStream("register.txt", {flags: 'a', encoding: null});
							stationArray.station1.studentIDout=studentID;
							stationArray.station1.assigned='0';
							stationArray.station1.bikeID='0000';
							var date= new Date();
							stationArray.station1.date1=date.getTime();
							console.log('Con estos campos: '+stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+'; \n\r');
							WriteRegister.write(stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+';');
							//se reinician las variables
							stationArray.station1.studentIDout='0000000000';
							stationArray.station1.assigned='a';
							stationArray.station1.bikeID='0000';
							stationArray.station1.date1='0';
							stationArray.station1.flag=0;							
							flag2=0;
							cont.contStudents=cont.contStudents+1;
							console.log('SE CREO USUARIO Y CONTSTUDENTS: '+cont.contStudents);
							console.log('SE CREO USUARIO Y CONTSTUDENTSPREV: '+contprev.contStudents);
								replace1({
								regex:day.toDateString()+','+contprev.contStudents+','+contprev.contFines+';',
								replacement: day.toDateString()+','+cont.contStudents+','+cont.contFines+';',
								paths: ['statistics.txt'],
								recursive: true,
								silent: true,
								});
							contprev.contStudents=cont.contStudents;
							contprev.contFines=cont.contFines;
					
							} else{
							//El usuario ya está en el registro y ahora se comprueba la variable assigned que está escrita en el register.txt
							
							stationArray.station1.studentIDout=studentID;
							var inicio=datos.indexOf(stationArray.station1.studentIDout);
							var init;
							var fin;
							//se extraen los datos del usuario del register.txt

								console.log('El inicio de studentID: '+inicio+'\n\r');
								init=datos.toString().indexOf(",",inicio+1);
								fin=datos.toString().indexOf(",",init+1);

								stationArray.station1.assigned=datos.toString().substring(init+1, fin);
								init=fin;
								fin=datos.toString().indexOf(",",init+1);

								stationArray.station1.bikeID=datos.toString().substring(init+1,fin);
								init=fin;
								fin=datos.toString().indexOf(";",init+1);

								stationArray.station1.date1=datos.toString().substring(init+1,fin);							

							console.log('Assigned Validation, actual user date: '+stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+'; \n\r');					
	
							console.log('entró a verificación de assgined: \n\r');							
							console.log('Queda a la espera de bikeID: \n\r');							
stationArray.station1.flag=1;
												
						
								} //Segundo if o final de else
		
							}); //Termina  función para leer en el archivo register

				}//primer id
			
								
				});//end reading file	
	//Termina de abrir archivo de multas fine.txt

					
				}else{
				console.log('Usted NO es un USUARIO válido');
				io.emit('audio', 'notvalid');
			//se reinician las variables
				stationArray.station1.studentIDout='0000000000';
				stationArray.station1.assigned='a';
				stationArray.station1.bikeID='0000';
				stationArray.station1.date1='0';
				stationArray.station1.flag=0;

					}

				});  //end reading file 
	//Termina comprobación de DataBase
			//}//if(datoprev...
		
		//Termina función dentro de recibir studentID
		}else{
		var WriteRegister=fs.createWriteStream("register.txt", {flags: 'w', encoding: null});
			WriteRegister.write('');
			console.log('Se sobreescribió el archivo register.txt por el Master |m|');
		}


	});


	//*****************************************Aquí empieza el socket de BikeID******************************************


	socket.on('bikeID', function(bikeID1){
	//Contadores de multas


	if (stationArray.station1.flag==1){
	console.log('Esto es assigned socket de bikeID: '+stationArray.station1.assigned+'\n\r');
	readRegister=fs.readFile("register.txt", function(err,chunk){
						datos='';						
						datos+=chunk;
						console.log('Aquí esto es studentId: '+stationArray.station1.studentIDout+'\n\r');
						
							//Pasa a comprobar assigned y a separar lo que se tiene en el archivo dd register
							var inicio=datos.indexOf(stationArray.station1.studentIDout);
							var init;
							var fin;

								init=datos.toString().indexOf(",",inicio+1);
								fin=datos.toString().indexOf(",",init+1);

								stationArray.station1.assigned=datos.toString().substring(init+1, fin);
								init=fin;
								fin=datos.toString().indexOf(",",init+1);

								stationArray.station1.bikeID=datos.toString().substring(init+1,fin);
								init=fin;
								fin=datos.toString().indexOf(";",init+1);

								stationArray.station1.date1=datos.toString().substring(init+1,fin);							

							console.log('Assigned Validation, actual user date: '+stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+'; \n\r');					
	
						
								if(stationArray.station1.assigned=='0'){
									//Assigned =0 entonces se espera al studentID del lector y al BikeID.Aquí está el problema					
					console.log('entró a assigned 0: \n\r');	
					console.log('Se cambiarán los campos de assigned a 1 y de bikeID al valor del tag: \n\r');
					var tempBike;	
					console.log(stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+'; \n\r');					
					var date= new Date();
					date2=date.getTime();
					replace1({
						regex:stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+';',
						replacement: stationArray.station1.studentIDout+','+'1'+','+bikeID1+','+date2+';',
						paths: ['register.txt'],
						recursive: true,
						silent: true,
						});
						//reinicia variables
						stationArray.station1.studentIDout='0000000000';
						stationArray.station1.assigned = 'a';
						stationArray.station1.bikeID='0000';
						stationArray.station1.flag=0;
																
									}else{
									//Assigned = 1 entonces se espera que el estudiante retorne la bicicleta al sistema y salga de él
								console.log('Usted devolverá la bicicleta. Se borrará su registro. Espere.');						console.log('esto es stationArray.station1.bikeID: '+stationArray.station1.bikeID+' y esto es bikeID1 del socket'+bikeID1+ '\n\r');																				
										
										if(stationArray.station1.bikeID==bikeID1){
										
										var date= new Date();
										date2=date.getTime();
										if(((date2-stationArray.station1.date1)/(1000*60))<=1){
									
									replace1({
									regex:stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+';',
									replacement: '',
									paths: ['register.txt'],
									recursive: true,
									silent: true,
									});
									stationArray.station1.studentIDout='';
									stationArray.station1.assigned='a';
									stationArray.station1.bikeID='0000';
									stationArray.station1.date1='';
									stationArray.station1.flag=0;
									
										 				}else{
												console.log('Su tiempo de uso fue mayor al permitido, se le asignará una multa que lo inhabilitará de usar el sistema hasta que sea paga \n\r');
									io.emit('audio', 'fineGeneration');
									cont.contFines++;
									
								replace1({
								regex:day.toDateString()+','+contprev.contStudents+','+contprev.contFines+';',
								replacement: day.toDateString()+','+cont.contStudents+','+cont.contFines+';',
								paths: ['statistics.txt'],
								recursive: true,
								silent: true,
								});
								contprev.contStudents=cont.contStudents;
								contprev.contFines=cont.contFines;
								
										
																							          replace1({
								
									regex:stationArray.station1.studentIDout+','+stationArray.station1.assigned+','+stationArray.station1.bikeID+','+stationArray.station1.date1+';',
	 								replacement: '',
									paths: ['register.txt'],
									recursive: true,
									silent: true,
									});
									var WriteFine=fs.createWriteStream("fine.txt", {flags: 'a', encoding: null});
									WriteFine.write(stationArray.station1.studentIDout+';'+'\n');
									stationArray.station1.studentIDout='';
									stationArray.station1.assigned='a';
									stationArray.station1.bikeID='0000';
									stationArray.station1.date1='';
									stationArray.station1.flag=0;
														}
									
									console.log('Gracias por usar BiciRun. \n\r ');	
										}else{
										console.log('Bicicleta incorrecta \n\r');
										io.emit('audio', 'wrongBike');
										} //aquí se cierra if de comparación de bikeID				
									//}// se cierra if del flag. Requiere que primero se pase el carné para devolver la bicicleta
						
									}// aqui se cierra el else
							});
	
}//if(flag==1)...
	}); //socket que recibe bikeID




}); //io connection



