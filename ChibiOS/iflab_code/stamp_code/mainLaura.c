#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"


static WORKING_AREA(waThread1, 128);

// Pin DIG 
static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
   	//palClearPad(IOPORT1, PIOA_DIG10); //apagado
	// chThdSleepMilliseconds(1000);
    	palSetPad(IOPORT1, PIOA_DIG10); //encendido
   	//chThdSleepMilliseconds(1000);
   
  }
  return(0);
}


//enviar comando 
void Sendcmd(unsigned char address,unsigned char cmd)
{ 
  chIOPut((BaseChannel *)&SD2,address);//set the address of SRF02(factory default is 0)
  chThdSleepMicroseconds(100);  
  chIOPut((BaseChannel *)&SD2,cmd); //send the command to SRF02
  chThdSleepMicroseconds(100); 
}


void main (void)
 {
  halInit();
  chSysInit();

  sdStart(&SD2, NULL);  /* Activates the serial driver 2	 */
  sdStart(&SD1, NULL);

  palSetPadMode(IOPORT1, PIOA_DIG10, PAL_MODE_OUTPUT_PUSHPULL);

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  unsigned char reading[10];

  while(1){
	
 	
	//msg_t reading;

	Sendcmd(0x85,0x0C);
	chThdSleepMilliseconds(70);
	for(int i=0; i<10; i++){
		reading[i] = chIOGet(&SD2);
	}
	chprintf((BaseChannel *)&SD1, " tag:");
	for(int i=0; i<5; i++){
		chprintf((BaseChannel *)&SD1, " %x", reading);
		chThdSleepMilliseconds(250);
	}
	}


 }




////////////////// ORIGINAL
/*
  unsigned char data [10] = {10 , 11 , 12 , 13 , 11 , 12 , 13 , 11 , 12 , 13};
	while(1){
	
    	for(int i = 0 ; i < 10 ; i++){
    	    chprintf((BaseChannel *)&SD2,"%d;", data[i] );
    	}	
*/



