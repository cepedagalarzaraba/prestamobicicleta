#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_adc.h"
#include "atmel_pwm.h"
#include "atmel_pio.h"
#include "atmel_usart.h"

#define BOARD_MCK               48000000
#define BUFFER_SIZE         1
#define MAX_BPS             500

const Pin pins[] = {
    PIN_USART0_RXD,
    PIN_USART0_TXD,
    PIN_USART0_CTS,
    PIN_USART0_RTS,
};


/**  Number of bytes received between two timer ticks.*/
volatile uint32_t bytesReceived = 0;

/**  Receive buffer.*/
uint8_t pBuffer[BUFFER_SIZE];

/**  String buffer.*/
char pString[24];
char dato;


void USART0_IrqHandler(void)
{
    uint32_t status;
    
    status = USART0->US_CSR;

    /* */
    if ((status & US_CSR_RXRDY) == US_CSR_RXRDY) {

       dato = USART0->US_RHR;}
        /* Otherwise disable interrupt*/
        else {

            USART0->US_IDR = US_IDR_RXBUFF;
        }    
  
}

static void _ConfigureUsart( void )
{
    uint32_t mode = US_MR_USART_MODE_NORMAL
                        | US_MR_USCLKS_MCK
                        | US_MR_CHRL_8_BIT
                        | US_MR_PAR_NO
                        | US_MR_NBSTOP_1_BIT
                        | US_MR_CHMODE_NORMAL ;

    /* Enable the peripheral clock in the PMC*/
    USART0_Initialize();

    /* Configure the USART in the desired mode @115200 bauds*/
    USART_Configure( USART0, mode, 115200, BOARD_MCK ) ;

    /* Configure the RXBUFF interrupt*/
    nvicEnableVector(USART0_IRQn , CORTEX_PRIORITY_MASK(SAM3_USART0_PRIORITY));

    /* Enable receiver & transmitter*/
    USART_SetTransmitterEnabled( USART0, 1 ) ;
    USART_SetReceiverEnabled( USART0, 1 ) ;
    
    chprintf((BaseChannel *)&SD1,"Configuracion Terminada\n\r");
}

CH_IRQ_HANDLER(USART0_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    USART0_IrqHandler();
    CH_IRQ_EPILOGUE();
}


void main (void)
 {
  
    halInit();
	chSysInit();
	sdStart(&SD1, NULL);  
	chprintf((BaseChannel *)&SD1,"Prueba USART\n\r");
	PIO_Configure( pins, PIO_LISTSIZE( pins ) ) ;
	_ConfigureUsart() ;
	//USART0->US_IER = ;
	USART_EnableIt(USART0, US_IDR_RXRDY);
	//USART_ReadBuffer( USART0, pBuffer, BUFFER_SIZE ) ;
    //unsigned char character;
    /* Infinite loop*/
    /*
    while ( 1 ){
        character = USART_GetChar(USART0);
        chprintf((BaseChannel *)&SD1,"Polling USART %d \n\r",character);
    }
	*/
	while(1){
	chprintf((BaseChannel *)&SD1,"Polling USART %d \n\r",dato);
	chThdSleepMilliseconds(100);
	USART_EnableIt(USART0, US_IDR_RXRDY);
	};
	
	
 }
