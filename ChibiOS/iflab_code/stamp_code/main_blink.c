/* 
 * File:   Main.c
 * Author: arturo
 *
 * Created on 18 de marzo de 2014, 09:49 PM
 * 
 * En este codigo se provara el comportamiento del controlador fuzzy, se tomara el modelo una planta y se evaluara la acción de 
 * control generada y el comportmiento del sistema. El modelo discreto de la planta a simular es:
 * Y(i+1) = 0.01709*U(i) + 0.9829*Y(i);
 */

#include <math.h>
#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
// Libreria creada para generar el consecuente y los antecedentes del
		      // Control Fuzzy 


static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
    palClearPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
    palSetPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
  }
  return(0);
}


void main (void)
 {
  
  halInit();
  chSysInit();
  sdStart(&SD1, NULL);  /* Activates the serial driver 2 */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  
  unsigned char data [10] = {10 , 11 , 12 , 13 , 11 , 12 , 13 , 11 , 12 , 13};
	while(1){
	
    	for(int i = 0 ; i < 10 ; i++){
    	    chprintf((BaseChannel *)&SD1,"%d;", data[i] );
    	}
		
	chprintf((BaseChannel *)&SD1,"%s-","hola mundo-blink" );
	}
 }

// *****************************************************************************************************
//******************************* 	FIN DE LA FUNCION PRINCIPAL


