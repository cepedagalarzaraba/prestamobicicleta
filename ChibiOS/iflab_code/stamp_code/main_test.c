/* 
 * File:   Main.c
 * Author: arturo
 *
 * Created on 18 de marzo de 2014, 09:49 PM
 * 
 * En este codigo se provara el comportamiento del controlador fuzzy, se tomara el modelo una planta y se evaluara la acción de 
 * control generada y el comportmiento del sistema. El modelo discreto de la planta a simular es:
 * Y(i+1) = 0.01709*U(i) + 0.9829*Y(i);
 */

#include <math.h>
#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
// Libreria creada para generar el consecuente y los antecedentes del
		      // Control Fuzzy 


static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
    //palClearPad(IOPORT2, PIOB_LED2);
    //chThdSleepMilliseconds(500);
    palSetPad(IOPORT1, PIOA_DIG10);
//chThdSleepMilliseconds(500);
  }
  return(0);
}


//enviar comando 
void Sendcmd(unsigned char address,unsigned char cmd)
{ 	
	palClearPad(IOPORT1, PIOA_DIG9);
	palClearPad(IOPORT1, PIOA_DIG8);
  	chIOPut((BaseChannel *)&SD2,0x00);//
	chIOPut((BaseChannel *)&SD2,address & 0x7E);//

	
	chIOPut((BaseChannel *)&SD2,cmd);//
	chIOPut((BaseChannel *)&SD2,0xFF);//
	palSetPad(IOPORT1, PIOA_DIG8);
}




unsigned char Readcmd(unsigned char address)
{
	unsigned char valor;
	chIOPut((BaseChannel *)&SD2,0x00);//
	chIOPut((BaseChannel *)&SD2,0x80 | (address & 0x7E));//
	valor=chIOGet(&SD2);
	chIOPut((BaseChannel *)&SD2,0xFF);//

	return valor;
}

void main (void)
 {
  
  halInit();
  chSysInit();
  palSetPadMode(IOPORT1, PIOA_DIG10, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPadMode(IOPORT1, PIOA_DIG9, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPadMode(IOPORT1, PIOA_DIG8, PAL_MODE_OUTPUT_PUSHPULL);
	chprintf((BaseChannel *)&SD2,"HOLA MUNDO");
    	



  sdStart(&SD1, NULL);
  sdStart(&SD2, NULL);  /* Activates the serial driver 2 */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  
  unsigned char rfid;
  unsigned char antena;
  unsigned char buff;

	while(1){
	
	//inicializar RFID
	Sendcmd(0x2A,0x80);//inicializar 
	Sendcmd(0x2B,0xA9);//timer
	Sendcmd(0x2C,0x03);//reload timer
	Sendcmd(0x2D,0xE8);//
	Sendcmd(0x15,0x40);//
	Sendcmd(0x11,0x3D);//

	antena=Readcmd(0x14);
	
	if((antena & 0x03) != 0x03){
		Sendcmd(0x14, antena | 0x03);	
	}
	// termina procemiento de inicializacion de sensor RFID



	
	//chThdSleepMilliseconds(70);
	//Sendcmd(0x01,0x08); //comando leer
	
	//chIOPut((BaseChannel *)&SD2,0x08);//
	//chThdSleepMilliseconds(10);  
	//buscar bien las direcciones

    	chprintf((BaseChannel *)&SD2,"%dSD2---tag:", rfid );
    		
	
	}
 }

// *****************************************************************************************************
//******************************* 	FIN DE LA FUNCION PRINCIPAL


