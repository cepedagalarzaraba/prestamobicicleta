/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2010, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

/**
 * \page twi_eeprom_pdc_irq TWI EEPROM PDC IRQ Example
 *
 * \section Purpose
 *
 * This basic example program demonstrates how to use the TWI peripheral
 * to access an external serial EEPROM chip.
 *
 * \section Requirements
 *
 * This package can be used with SAM3N evaluation kits.
 * Connect the TWI0 peripheral to eeprom device:
 *        - <b>SAM3N-- EEPROM</b>
 *        - VCC        -- VCC
 *        - GND        -- GND
 *        - TWD0 (PA3) -- SDA
 *        - TWCK0(PA4) -- SCK
 *
 * Please note that two pull-up resistors must be connected to a positive
 * supply voltage on the TWD0 and TWCK0.
 *
 * \section Description
 *
 * The code can be roughly broken down as follows:
 * <ul>
 * <li>Configure TWI pins.</li>
 * <li>Enable TWI peripheral clock.</li>
 * <li>Configure TWI clock.</li>
 * <li>Initialize TWI as twi master.</li>
 * <li>TWI interrupt handler.</li>
 * <li>The main function, which implements the program behavior.</li>
 * <ol>
 * <li>Sets the first and second page of the EEPROM to all zeroes.</li>
 * <li>Writes pattern in page 0. </li>
 * <li>Reads back data in page 0 and compare with original pattern (polling). </li>
 * <li>Writes pattern in page 1. </li>
 * <li>Reads back data in page 1 and compare with original pattern (interrupts). </li>
 * </ol>
 * </ul>
 *
 * \section Usage
 *
 * -# Build the program and download it inside the evaluation board. Please
 *    refer to the
 *    <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6224.pdf">
 *    SAM-BA User Guide</a>, the
 *    <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6310.pdf">
 *    GNU-Based Software Development</a> application note or to the
 *    <a href="ftp://ftp.iar.se/WWWfiles/arm/Guides/EWARM_UserGuide.ENU.pdf">
 *    IAR EWARM User Guide</a>, depending on your chosen solution.
 * -# On the computer, open and configure a terminal application
 *    (e.g. HyperTerminal on Microsoft Windows) with these settings:
 *   - 115200 bauds
 *   - 8 bits of data
 *   - No parity
 *   - 1 stop bit
 *   - No flow control
 * -# Start the application.
 * -# In the terminal window, the following text should appear:
 *    \code
 *     -- TWI EEPROM PDC IRQ Example xxx --
 *     -- xxxxxx-xx
 *     -- Compiled: xxx xx xxxx xx:xx:xx --
 *    \endcode
 * -# The following traces detail operations on the EEPROM, displaying success
 *    or error messages depending on the results of the commands.
 *
 * \section References
 * - twi_eeprom_pdc_irq/main.c
 * - twi.c
 * - twi.h
 * - twid.c
 * - twid.h
 */

/**
 * \file
 *
 * This file contains all the specific code for the twi eeprom pdc irq example.
 */

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

//#include "board.h"

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_twi.h"
#include "atmel_twid.h"
#include "atmel_pio.h"


#include "async.h"

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>

/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/

/** TWI clock frequency in Hz. */
#define TWCK            400000

/** Slave address of AT24C chips.*/
#define AT24C_ADDRESS   0x50

/** Page size of an AT24C512 chip (in bytes)*/
#define PAGE_SIZE       64

#define BOARD_PINS_TWI_EEPROM   PINS_TWI0

#define BOARD_ID_TWI_EEPROM     ID_TWI0

#define BOARD_MCK               48000000


/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/
/** Pio pins to configure. */
static const Pin pins[] = {
	PIN_TWD0,
	PIN_TWCK0
};

/** TWI driver instance.*/
static Twid twid;

/** Page buffer.*/
static uint8_t pData[PAGE_SIZE];
static uint8_t singleByte;

/*----------------------------------------------------------------------------
 *        Local functions
 *----------------------------------------------------------------------------*/
/**
 * \brief TWI interrupt handler. Forwards the interrupt to the TWI driver handler.
 */


/**
 * \brief Dummy callback, to test asynchronous transfer modes.
 */
static void TestCallback( void )
{
    chprintf((BaseChannel *)&SD2, "-I- Callback fired !\n\r" ) ;
}


CH_IRQ_HANDLER(TWI0_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    TWI0_IrqHandler();
    CH_IRQ_EPILOGUE();
}

/*----------------------------------------------------------------------------
 *        Global functions
 *----------------------------------------------------------------------------*/

/**
 * \brief Application entry point for TWI eeprom example.
 *
 * \return Unused (ANSI-C compatibility).
 */

extern int main( void )
{
    uint32_t i;
    uint32_t numErrors;

	halInit();
	chSysInit();
	sdStart(&SD1, NULL);  

	chThdSleepMilliseconds(1500);
	chprintf((BaseChannel *)&SD1,"Starting TWI test\n\r");


    /* Configure TWI pins. */
    PIO_Configure(pins, PIO_LISTSIZE(pins));

    /* Enable TWI peripheral clock */
    TWI0_Initialize();
    /* Configure TWI */
    TWI_ConfigureMaster(TWI0, TWCK, BOARD_MCK);
    TWID_Initialize(&twid, TWI0);

	nvicEnableVector(TWI0_IRQn , CORTEX_PRIORITY_MASK(SAM3_TWI0_PRIORITY));


    /* Programa de comunicación con el sensor */

    //Enviar comando comSelectCard[]={1,1}
    unsigned char ComSelectCard[]={1,1};
    unsigned char g_cRxBuf;	    
    ///
    while(1){
    memset(ComSelectCard, 0, PAGE_SIZE);
    chprintf((BaseChannel *)&SD1,"Escribiendo un comando\n\r");
    TWID_Write(&twid, AT24C_ADDRESS, 0xa0, 1, ComSelectCard, 2, 0);
    chThdSleepMilliseconds(10);
    TWID_Read(&twid, AT24C_ADDRESS, 0xa0|1, 1, &g_cRxBuf, 1, 0);
    ///
    chThdSleepMilliseconds(10);
    chprintf((BaseChannel *)&SD1,"Respuesta %d\n\r", g_cRxBuf);
    chThdSleepMilliseconds(500);
    }
























	
}
