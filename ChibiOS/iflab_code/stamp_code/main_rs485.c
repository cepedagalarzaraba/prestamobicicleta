#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_adc.h"
#include "atmel_pwm.h"
#include "atmel_pio.h"
#include "atmel_usart.h"
#include <stdio.h>
#include <string.h>

/** Size of the receive buffer and transmit buffer. */
#define BUFFER_SIZE         11

/** Size of the pdc buffer. */
#define PDC_BUF_SIZE    (BUFFER_SIZE)

/** Acknowledge time out in ms */
#define TIMEOUT             (1000)

/** Character to synchronize with the other end */
#define SYNC_CHAR             0x11

/** Character to acknowledge receipt of the sync char */
#define ACK_CHAR             0x13

/** baud rate*/
#define BAUDRATE_RS485      256000

/** state of the usart*/
typedef enum _tUSART_STATE
{
    INITIALIZED,
    TRANSMITTING,
    RECEIVING,
    RECEIVED,
    TRANSMITTED
} tUSART_STATE ;

const Pin pins[] = {
    PIN_USART0_RXD,
    PIN_USART0_TXD,
    PIN_USART0_CTS,
    PIN_USART0_RTS,
};



/** temporary pointer for transmitting */
uint8_t* pBufTrans ;

/** temporary pointer for receiving */
uint8_t* pBufRecv ;

/** Transmit buffer. Pure ASCII text */
uint8_t pBufferTransmit[BUFFER_SIZE] = "Hello TX/RX"

/**  Number of bytes received between two timer ticks.*/
volatile uint32_t bytesReceived = 0;



void USART0_IrqHandler( void )
{
    uint32_t status;
    status = USART_GetStatus( USART0 ) ;

    /* receiving interrupt */
    if ( (status & US_CSR_ENDRX) == US_CSR_ENDRX && state == RECEIVING )
    {
        /* partly received */
        if ( (uint32_t) pBufRecv < (uint32_t) (pBufferReceive + BUFFER_SIZE) )
        {
            if ( (status & US_CSR_ENDRX) == US_CSR_ENDRX )
            {
                pBufRecv += PDC_BUF_SIZE ;
            }

            /* prepare for next reception */
            RS485_USART_ReadBuffer( USART0, pBufRecv, PDC_BUF_SIZE ) ;
            USART_EnableIt( USART0, US_IER_ENDRX ) ;
        }
        else
        {
            /* indicate receiving finished */
            state = RECEIVED ;
            USART_DisableIt( USART0, US_IDR_ENDRX ) ;
        }
    }
    /* transmitting interrupt */
    else
    {
        if ((status & US_CSR_ENDTX) == US_CSR_ENDTX && state == TRANSMITTING)
        {
            /* transmit continuously */
            if (((uint32_t) pBufTrans) < (uint32_t) (pBufferTransmit + BUFFER_SIZE))
            {
                pBufTrans += PDC_BUF_SIZE;
                RS485_USART_WriteBuffer(USART0, pBufTrans, PDC_BUF_SIZE);
                USART_EnableIt(USART0, US_IER_ENDTX);

            }
            else
            {
                state = TRANSMITTED;
                USART_DisableIt(USART0, US_IDR_ENDTX);
            }
        }
    }

}


static void _ConfigureUsart( void )
{
    uint32_t mode = US_MR_USART_MODE_RS485 | US_MR_USCLKS_MCK
                    | US_MR_CHRL_8_BIT | US_MR_PAR_NO | US_MR_NBSTOP_1_BIT
                    | US_MR_CHMODE_NORMAL;

    /* Enable the peripheral clock in the PMC*/
    USART0_Initialize();

    /* Configure USART0 in the desired mode @256000 bauds*/
    USART_Configure( USART0, mode, BAUDRATE_RS485, BOARD_MCK ) ;

    /* Disable all the interrupts*/
    USART0->US_IDR = 0xFFFFFFFF ;

    /* Enable USART0 interrupt*/
    nvicEnableVector(USART0_IRQn , CORTEX_PRIORITY_MASK(SAM3_USART0_PRIORITY));
}



CH_IRQ_HANDLER(USART0_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    USART0_IrqHandler();
    CH_IRQ_EPILOGUE();
}


void main (void)
 {
  
    halInit();
	chSysInit();
	sdStart(&SD2, NULL);  
	
	uint8_t cSync = SYNC_CHAR;
    uint32_t time_elapsed = 0;
    volatile uint32_t status = 0x0;
    int i ;
	
	pBufTrans = pBufferTransmit;
    pBufRecv = pBufferReceive;

    /* Configure pins to USART peripheral */
    PIO_Configure( pins, PIO_LISTSIZE( pins ) ) ;
    
    /* Configure USART*/
    _ConfigureUsart() ;
    
    /* initialize receiving buffer to distinguish with the sent frame*/
    memset( pBufferReceive, 0x0, sizeof( pBufferReceive ) ) ;
    
    /* Read status register */
    status = USART_GetStatus( USART0 ) ;

    /* Enable transmitter here*/
    USART_SetTransmitterEnabled( USART0, 1 ) ;
    /* disable receiver first,to avoid receiving characters sent by itself*/
    /* It's necessary for half duplex RS485*/
    USART_SetReceiverEnabled( USART0, 0 ) ;

    /* send a sync character */
    RS485_USART_WriteBuffer( USART0, &cSync, 1 ) ;
	
	chThdSleepMilliseconds(50);
	
	RS485_USART_ReadBuffer( USART0, &cSync, 1 ) ;

    /* status register*/
    status = USART_GetStatus( USART0 ) ;

    /* then enable receiver*/
    USART_SetReceiverEnabled(USART0,1);

    /* wait until time out or acknowledgment is received*/
    time_elapsed = chTimeNow() ;
    while ( (status & US_CSR_ENDRX) != US_CSR_ENDRX )
    {
        status = USART_GetStatus( USART0 ) ;
        if ( chTimeNow() - time_elapsed > TIMEOUT )
        {
            break ;
        }
    }

    /* if acknowledgment received in a short time*/
    if ( (status & US_CSR_ENDRX) == US_CSR_ENDRX )
    {
        /* Acknowledgment*/
        if ( cSync == ACK_CHAR )
        {
            /*act as transmitter, start transmitting*/
            state = TRANSMITTING ;
            chprintf((BaseChannel *)&SD2, "-I- Start transmitting!\n\r" ) ;
            RS485_USART_WriteBuffer( USART0, pBufTrans, PDC_BUF_SIZE ) ;

            pBufTrans += PDC_BUF_SIZE ;
            /* enable transmitting interrupt */
            USART_EnableIt( USART0, US_IER_ENDTX ) ;
        }
    }
    else
    {
        /* start receiving*/
        /* act as receiver*/
        RS485_USART_ReadBuffer( USART0, &cSync, 1 ) ;
        status = USART_GetStatus( USART0 ) ;

        chprintf((BaseChannel *)&SD2, "-I- Receiving sync character.\n\r" ) ;
        while ( (status & US_CSR_ENDRX) != US_CSR_ENDRX )
        {
            status = USART_GetStatus( USART0 ) ;
        }

        /* sync character is received */
        if ( cSync == SYNC_CHAR )
        {
            /* SEND XOff AS acknowledgment */
            cSync = ACK_CHAR ;

            /* Delay to prevent the character from being discarded by transmitter due to responding too soon */
            Wait( 100 ) ;
            RS485_USART_WriteBuffer( USART0, &cSync, 1 ) ;
            state = RECEIVING ;

            chprintf((BaseChannel *)&SD2, "-I- Start receiving!\n\r" ) ;
            RS485_USART_ReadBuffer( USART0, pBufRecv, PDC_BUF_SIZE ) ;
            pBufRecv += PDC_BUF_SIZE ;

            /* enable receiving interrupt */
            USART_EnableIt( USART0, US_IER_ENDRX ) ;
        }
    }
    while ( state != RECEIVED ) ;
    Wait( 200 ) ;

    i = 0 ;
    /* print received frame out*/
    while ( i < BUFFER_SIZE && pBufferReceive[i] != '\0' )
    {
        if ( pBufferTransmit[i] != pBufferReceive[i] )
        {
            chprintf((BaseChannel *)&SD2, "-E- Error occurred while receiving!\n\r" ) ;
            /* infinite loop here*/
            while( 1 ) ;
        }
        i++ ;
    }
    chprintf((BaseChannel *)&SD2, "-I- Received successfully!\n\r" ) ;

    while( 1 ) ;

	
 }
