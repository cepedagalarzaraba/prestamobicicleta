/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2010, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

/**
 * \page twi_eeprom_pdc_irq TWI EEPROM PDC IRQ Example
 *
 * \section Purpose
 *
 * This basic example program demonstrates how to use the TWI peripheral
 * to access an external serial EEPROM chip.
 *
 * \section Requirements
 *
 * This package can be used with SAM3N evaluation kits.
 * Connect the TWI0 peripheral to eeprom device:
 *        - <b>SAM3N-- EEPROM</b>
 *        - VCC        -- VCC
 *        - GND        -- GND
 *        - TWD0 (PA3) -- SDA
 *        - TWCK0(PA4) -- SCK
 *
 * Please note that two pull-up resistors must be connected to a positive
 * supply voltage on the TWD0 and TWCK0.
 *
 * \section Description
 *
 * The code can be roughly broken down as follows:
 * <ul>
 * <li>Configure TWI pins.</li>
 * <li>Enable TWI peripheral clock.</li>
 * <li>Configure TWI clock.</li>
 * <li>Initialize TWI as twi master.</li>
 * <li>TWI interrupt handler.</li>
 * <li>The main function, which implements the program behavior.</li>
 * <ol>
 * <li>Sets the first and second page of the EEPROM to all zeroes.</li>
 * <li>Writes pattern in page 0. </li>
 * <li>Reads back data in page 0 and compare with original pattern (polling). </li>
 * <li>Writes pattern in page 1. </li>
 * <li>Reads back data in page 1 and compare with original pattern (interrupts). </li>
 * </ol>
 * </ul>
 *
 * \section Usage
 *
 * -# Build the program and download it inside the evaluation board. Please
 *    refer to the
 *    <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6224.pdf">
 *    SAM-BA User Guide</a>, the
 *    <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6310.pdf">
 *    GNU-Based Software Development</a> application note or to the
 *    <a href="ftp://ftp.iar.se/WWWfiles/arm/Guides/EWARM_UserGuide.ENU.pdf">
 *    IAR EWARM User Guide</a>, depending on your chosen solution.
 * -# On the computer, open and configure a terminal application
 *    (e.g. HyperTerminal on Microsoft Windows) with these settings:
 *   - 115200 bauds
 *   - 8 bits of data
 *   - No parity
 *   - 1 stop bit
 *   - No flow control
 * -# Start the application.
 * -# In the terminal window, the following text should appear:
 *    \code
 *     -- TWI EEPROM PDC IRQ Example xxx --
 *     -- xxxxxx-xx
 *     -- Compiled: xxx xx xxxx xx:xx:xx --
 *    \endcode
 * -# The following traces detail operations on the EEPROM, displaying success
 *    or error messages depending on the results of the commands.
 *
 * \section References
 * - twi_eeprom_pdc_irq/main.c
 * - twi.c
 * - twi.h
 * - twid.c
 * - twid.h
 */

/**
 * \file
 *
 * This file contains all the specific code for the twi eeprom pdc irq example.
 */

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

//#include "board.h"

#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_twi.h"
#include "atmel_twid.h"
#include "atmel_pio.h"


#include "async.h"

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>

/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/

/** TWI clock frequency in Hz. */
#define TWCK            100000

/** Slave address of AT24C chips.*/
#define ADDR   0x50

/** Page size of an AT24C512 chip (in bytes)*/
#define PAGE_SIZE       64

#define BOARD_PINS_TWI_EEPROM   PINS_TWI0

#define BOARD_ID_TWI_EEPROM     ID_TWI0

#define BOARD_MCK               48000000


/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/
/** Pio pins to configure. */
static const Pin pins[] = {
	PIN_TWD0,
	PIN_TWCK0
};

/** TWI driver instance.*/
static Twid twid;
static uint8_t dato[]={0x01,0xF0};
static uint8_t out[12];


/*----------------------------------------------------------------------------
 *        Local functions
 *----------------------------------------------------------------------------*/
/**
 * \brief TWI interrupt handler. Forwards the interrupt to the TWI driver handler.
 */


/**
 * \brief Dummy callback, to test asynchronous transfer modes.
 */
static void TestCallback( void )
{
    chprintf((BaseChannel *)&SD2,(BaseChannel *)&SD2, "-I- Callback fired !\n\r" ) ;
}


CH_IRQ_HANDLER(TWI0_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    TWI0_IrqHandler();
    CH_IRQ_EPILOGUE();
}

/*----------------------------------------------------------------------------
 *        Global functions
 *----------------------------------------------------------------------------*/
uint8_t readID()
{
	// write SELECT command
	uint8_t in[]={0x01,0x01};
	TWID_Write(&twid, ADDR, 0, 0, in, 2, 0);
	chThdSleepMilliseconds(400);

	// Wait for response
	while(!palReadPad(IOPORT1, PIOA_DIG10)){
    		// Allow some time to respond
		chThdSleepMilliseconds(5);
    
		// Anticipate maximum packet size
		TWID_Read(&twid, ADDR, 0,0, out, 11, 0);
		palSetPad(IOPORT1, PIOA_DIG9);
		chThdSleepMilliseconds(1000);
		
			// Read command code, should be same as request (1)
			if(out[1] != 1) return -1;

			// Read status
			switch(out[2])
      			{
        			case 0: // Succes, read ID and tag type
        			{
          			// Get tag ID
					chprintf((BaseChannel *)&SD1,"%X%X%X%X;",out[3],out[4],out[5],out[6]);
		       		}
       			return 1;

      			case 0x0A: // Collision
        			chprintf((BaseChannel *)&SD1,"Collision detected");
        		break;

      			case 1: // No tag
        			chprintf((BaseChannel *)&SD1,"No tag found");
        		break;

      			default: // Unexpected
        			chprintf((BaseChannel *)&SD1,"Unexpected result");
      			}
		return -1;
    		
	}
  	// No tag found or no response
	return 0;
}

void ledOn(uint8_t on)
{
  // Send LED command
	uint8_t led[]={0x02,0x40,on};
	TWID_Write(&twid, 0x50, 0, 0, led , 3, 0);
        chThdSleepMilliseconds(2500);
        TWID_Read(&twid, 0x50, 0, 0, out, 3,0);
	chThdSleepMilliseconds(250);
        chprintf((BaseChannel *)&SD1,"read:%X-%X-%X",out[0],out[1],out[2]);

}

void version()
{
  // Send LED command
	uint8_t V[]={0x01,0xF0};
	TWID_Write(&twid, 0x50, 0, 0, V , 2, 0);
        chThdSleepMilliseconds(2500);
        TWID_Read(&twid, 0x50, 0, 0, out, 12,0);
	chThdSleepMilliseconds(250);
        chprintf((BaseChannel *)&SD1,"version:%X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X-%X",out[0],out[1],out[2],out[3],out[4],out[5],out[6],out[7],out[8],out[9],out[10],out[11]);

}


/**
 * \brief Application entry point for TWI eeprom example.
 *
 * \return Unused (ANSI-C compatibility).
 */


extern int main( void )
{
    

	halInit();
	chSysInit();
	palSetPadMode(IOPORT1, PIOA_DIG10, PAL_MODE_OUTPUT_PUSHPULL);
	palSetPadMode(IOPORT1, PIOA_DIG9, PAL_MODE_OUTPUT_PUSHPULL);
	sdStart(&SD1, NULL);  
	sdStart(&SD2, NULL);

    /* Configure TWI pins. */
	PIO_Configure(pins, PIO_LISTSIZE(pins));

    /* Enable TWI peripheral clock */
	TWI0_Initialize();
    /* Configure TWI */
	TWI_ConfigureMaster(TWI0, TWCK, BOARD_MCK);
	TWID_Initialize(&twid, TWI0);

	nvicEnableVector(TWI0_IRQn , CORTEX_PRIORITY_MASK(SAM3_TWI0_PRIORITY));
	
	
	// Wait for tag
  	while(1){
		palClearPad(IOPORT1, PIOA_DIG9);
		// Wait for tag
	  	while(palReadPad(IOPORT1, PIOA_DIG10));

	  	//Read tag ID
	  	readID();

	  	// Wait until tag is gone
	  	while(!palReadPad(IOPORT1, PIOA_DIG10));

	}

}
