#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_adc.h"
#include "atmel_pwm.h"

#include "microshell2.h"

#define PWM_FREQUENCY               1000
#define MAX_DUTY_CYCLE              50
#define MIN_DUTY_CYCLE              0


int16_t adc_values[1] = { 0 } ;
volatile unsigned char conversionDone = 0 ;


static WORKING_AREA(waShell, 2048);

/* shell thread */
static msg_t shell(void *arg)
{
	(void) arg;
	chRegSetThreadName("shell");
	start_shell();
}



static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
    palClearPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
    palSetPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
  }
  return(0);
}


static void serve_adc_interrupt(void){
    uint32_t status;
    status = ADC_GetStatus(ADC);
    if ( (status & ADC_ISR_RXBUFF) == ADC_ISR_RXBUFF )
    {
        conversionDone = 1;
        ADC_ReadBuffer( ADC, adc_values, 1 ) ;  //Lee solo un valor
    }
}


CH_IRQ_HANDLER(ADC_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    serve_adc_interrupt();
    CH_IRQ_EPILOGUE();
}



/*
 * Application entry point.
 */
int main(void) {
   int status ;

   Thread *shelltp = NULL;


  halInit();
  chSysInit();

  sdStart(&SD2, NULL);  /* Activates the serial driver 2 */

  /* ADC configuration*/
  ADC_Initialize( ADC);
  /* startup = 15:    640 periods of ADCClock
   * for prescal = 11
   *     prescal: ADCClock = MCK / ( (PRESCAL+1) * 2 ) => 48MHz / ((11+1)*2) = 2MHz
   *     ADC clock = 2 MHz
   */
  ADC_cfgFrequency( ADC, 15, 11 ); //
  ADC_check( ADC, 48000000 ); // Board Clock 48000000
  ADC_EnableChannel(ADC, ADC_CHANNEL_4);
  nvicEnableVector(ADC_IRQn, CORTEX_PRIORITY_MASK(SAM3_ADC_PRIORITY)); /* Enable ADC interrupt */
  ADC_StartConversion(ADC); /* Start conversion */
  ADC_ReadBuffer(ADC,adc_values,1);
  ADC_EnableIt(ADC,ADC_IER_RXBUFF); /* Enable PDC channel interrupt */

  /*PWM configuration*/


  /* Creates the blinker thread. */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  chprintf((BaseChannel *)&SD2, "ADC TEST STAMP IFLAB \r\n");


  while (TRUE) {

    if (!shelltp){
      shelltp = chThdCreateStatic(waShell, sizeof(waShell), NORMALPRIO, shell, NULL);
    }
    else if (chThdTerminated(shelltp)) {
      shelltp = NULL;           /* Triggers spawning of a new shell.   */
    }


    status = ADC_GetStatus( ADC ) ;
    if ( (status & ADC_ISR_EOC4) == ADC_ISR_EOC4 ) /* if conversion is done*/
      ADC_StartConversion( ADC ) ;

    chThdSleepMilliseconds(500);
    while ( !conversionDone ) {    chprintf((BaseChannel *)&SD2, ". \r\n");};
      if ( conversionDone )
      {
//         chprintf((BaseChannel *)&SD2, "%d \r\n", adc_values[0]*3300/1023 ) ;
         conversionDone = 0 ;
      }
  }

  return(0);
}
