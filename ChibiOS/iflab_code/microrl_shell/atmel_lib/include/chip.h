/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2010, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */
#ifndef _LIB_SAM3N_
#define _LIB_SAM3N_

/*
 * Peripherals registers definitions
 */
#include "SAM3N.h"

#define WEAK __attribute__ ((weak))
#define NO_INIT

/*
 * Core
 */
#include "exceptions.h"
//#include "core_cm3.h"


/*
 * Peripherals
 */
#include "adc.h"
//#include "async.h"
//#include "dacc.h"
//#include "efc.h"
//#include "flashd.h"
//#include "pio.h"
//#include "pio_it.h"
//#include "pmc.h"
//#include "pwmc.h"
//#include "rtc.h"
//#include "rtt.h"
//#include "spi.h"
//#include "trace.h"
//#include "tc.h"
//#include "twi.h"
//#include "twid.h"
//#include "usart.h"
//#include "wdt.h"

#endif /* _LIB_SAM3N_ */
