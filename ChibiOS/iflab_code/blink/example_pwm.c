/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2010, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

/**
 * \page pwm_led PWM with LED Example
 *
 * \section Purpose
 *
 * This example demonstrates a simple configuration of three PWM channels to
 * generate variable duty cycle signals.
 * This will cause three LEDs on the evaluation kit to glow repeatedly.
 *
 * \section Requirements
 *
 * This package can be used with SAM3N evaluation kits.
 *
 * \section Description
 *
 * Three PWM channels (channel #0, #1 and #2) are configured to generate
 * a 1kHz PWM signal.
 * On channel #0, the signal is configured as left-aligned.
 * On channel #1, the signal is center-aligned and the polarity is inverted.
 * On channel #2, the signal is configured as left-aligned and the polarity is inverted.
 *
 * \section Usage
 *
 * -# Build the program and download it inside the evaluation board. Please refer to the
 * <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6132.pdf">
 * SAM-BA User Guide</a>, the
 * <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6132.pdf">
 * GNU-Based Software Development</a> application note or to the
 * <a href="http://www.atmel.com/dyn/resources/prod_documents/doc6132.pdf">
 * IAR EWARM User Guide</a>, depending on your chosen solution.
 * -# Optionally, on the computer, open and configure a terminal application
 *    (e.g. HyperTerminal on Microsoft Windows) with these settings:
 *   - 115200 bauds
 *   - 8 bits of data
 *   - No parity
 *   - 1 stop bit
 *   - No flow control
 * -# Start the application.
 * -# Three LEDs will start glowing repeatedly.
 *
 * \section References
 * - pwm_led/main.c
 * - pwmc.c
 * - pwmc.h
 */

/**
 * \file
 *
 * This file contains all the specific code for the pwm_led example.
 */

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "board.h"

#include <stdint.h>
#include <stdio.h>

/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/

/** PWM frequency in Hz. */
#define PWM_FREQUENCY               1000

/** Maximum duty cycle value. */
#define MAX_DUTY_CYCLE              50
/** Minimum duty cycle value. */
#define MIN_DUTY_CYCLE              0

/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/

/** Pio pins to configure. */
static const Pin pins[] = {
    PIN_PWM_LED0,
    PIN_PWM_LED1,
    PIN_PWM_LED2
};

/*----------------------------------------------------------------------------
 *        Local functions
 *----------------------------------------------------------------------------*/

/**
 * \brief Interrupt handler for the PWM controller.
 */
void PWM_IrqHandler(void)
{
    static uint32_t count = 0;
    static uint32_t duty = MIN_DUTY_CYCLE;
    static uint8_t  fadeIn = 1;

    /* Interrupt on CHANNEL_PWM_LED0 */
    if ((PWM->PWM_ISR & (0x01 << CHANNEL_PWM_LED0)) == (0x01 << CHANNEL_PWM_LED0)) {

        count++;

        /* Fade in/out */
        if (count == (PWM_FREQUENCY / (MAX_DUTY_CYCLE - MIN_DUTY_CYCLE))) {

            /* Fade in */
            if (fadeIn) {

                duty++;
                if (duty == MAX_DUTY_CYCLE) {

                    fadeIn = 0;
                }
            }
            /* Fade out */
            else {

                duty--;
                if (duty == MIN_DUTY_CYCLE) {

                    fadeIn = 1;
                }
            }

            /* Set new duty cycle */
            count = 0;
            PWMC_SetDutyCycle(CHANNEL_PWM_LED0, duty);
            PWMC_SetDutyCycle(CHANNEL_PWM_LED1, duty);
            PWMC_SetDutyCycle(CHANNEL_PWM_LED2, duty);
        }
    }
}

/*----------------------------------------------------------------------------
 *         Global functions
 *----------------------------------------------------------------------------*/

/**
 * \brief Application entry point for PWM with LED example.
 * Outputs PWM waves on LEDs to make them fade in and out.
 *
 * \return Unused (ANSI-C compatibility).
 */
int main(void)
{
    /* Disable watchdog */
    WDT_Disable( WDT ) ;

    /* Output example information */
    printf("-- PWM with LED Example %s --\n\r", SOFTPACK_VERSION);
    printf("-- %s\n\r", BOARD_NAME);
    printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

    /* PIO configuration */
    PIO_Configure(pins, PIO_LISTSIZE(pins));

    /* Enable PWMC peripheral clock */
    PMC_EnablePeripheral(ID_PWM);

    /* Set clock A to run at PWM_FREQUENCY * MAX_DUTY_CYCLE (clock B is not used) */
    PWMC_ConfigureClocks(PWM_FREQUENCY * MAX_DUTY_CYCLE, 0, BOARD_MCK);

    /* Configure PWMC channel for LED0 (left-aligned) */
    PWMC_ConfigureChannel(CHANNEL_PWM_LED0, PWM_CMR_CPRE_CLKA, 0, 0);
    PWMC_SetPeriod(CHANNEL_PWM_LED0, MAX_DUTY_CYCLE);
    PWMC_SetDutyCycle(CHANNEL_PWM_LED0, MIN_DUTY_CYCLE);

    /* Configure PWMC channel for LED1 (center-aligned, inverted polarity) */
    PWMC_ConfigureChannel(CHANNEL_PWM_LED1, PWM_CMR_CPRE_CLKA, PWM_CMR_CALG, PWM_CMR_CPOL);
    PWMC_SetPeriod(CHANNEL_PWM_LED1, MAX_DUTY_CYCLE);
    PWMC_SetDutyCycle(CHANNEL_PWM_LED1, MIN_DUTY_CYCLE);

    /* Configure PWMC channel for LED2 (left-aligned, inverted polarity) */
    PWMC_ConfigureChannel(CHANNEL_PWM_LED2, PWM_CMR_CPRE_CLKA, 0, PWM_CMR_CPOL);
    PWMC_SetPeriod(CHANNEL_PWM_LED2, MAX_DUTY_CYCLE);
    PWMC_SetDutyCycle(CHANNEL_PWM_LED2, MIN_DUTY_CYCLE);

    /* Configure interrupt */
    NVIC_DisableIRQ(PWM_IRQn);
    NVIC_ClearPendingIRQ(PWM_IRQn);
    NVIC_SetPriority(PWM_IRQn, 0);
    NVIC_EnableIRQ(PWM_IRQn);
    PWMC_EnableChannelIt(CHANNEL_PWM_LED0);

    /* Enable channels for LEDs */
    PWMC_EnableChannel(CHANNEL_PWM_LED0);
    PWMC_EnableChannel(CHANNEL_PWM_LED1);
    PWMC_EnableChannel(CHANNEL_PWM_LED2);

    /* Infinite loop */
    while (1);
}

